import React from 'react';

const CertificateItem = ({img}) => {
    return (
        <div className="certificate__item">
            <img src={img} alt="" className="certificate__img"/>
        </div>
    );
};

export default CertificateItem;