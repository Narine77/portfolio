import React from 'react';
import Slider from "react-slick";
import  "./slick.css";
import "./slicktheme.css";
import {certificates} from "../../data";
import CertificateItem from "../CertificateItem";
import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";

const SimpleSlider = () => {
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        // autoplay: true,
    };
    return (
        <Slider {...settings}>
            {certificates.map((item) => {
                return <CertificateItem key={item.id} {...item}/>
            })}
        </Slider>
    );
};

export default SimpleSlider;