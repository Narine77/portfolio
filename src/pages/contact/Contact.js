import React from 'react';
import {
    FaEnvelopeOpen,
    FaPhoneSquareAlt,
    FaFacebookF,
    FaLinkedin, FaTelegram
} from "react-icons/fa";

import "./contact.css"

const Contact = () => {
    return (
        <section className='contact section'>
            <h2 className="section__title">
                Get In <span>Touch</span>
            </h2>
            <div className="contact__container container grid">
                <div className="contact_data">
                    <h3 className="contact__title">Contact</h3>
                    <div className="contact__info">
                        <div className="info__item" data-aos="fade-right" data-aos-duration="1000">
                            <FaEnvelopeOpen className="info__icon"/>
                            <div>
                                <span className="info__title">Mail me</span>
                                <h4 className="info__desc">n.mkrtchyan77@gmail.com</h4>
                            </div>
                        </div>
                        <div className="info__item" data-aos="fade-right" data-aos-duration="1000">
                            <FaPhoneSquareAlt className="info__icon"/>
                            <div>
                                <span className="info__title">Call me</span>
                                <h4 className="info__desc">+37494091415</h4>
                            </div>
                        </div>
                    </div>
                    <div className="contact__socials">
                        <a href="https://www.facebook.com/profile.php?id=100005719877274"
                           className="contact__social-link">
                            <FaFacebookF/>
                        </a>
                        <a href="https://telegram.me/MkrtchyanNarine" className="contact__social-link">
                            <FaTelegram/>
                        </a>
                        <a href="https://www.linkedin.com/in/narine-mkrtchyan-116556244/"
                           className="contact__social-link">
                            <FaLinkedin/>
                        </a>
                    </div>
                </div>
                <div className="contact_data">
                    <h3 className="contact__title">Address</h3>
                    <div className="contact__info">
                        <div className="info__item" data-aos="fade-right" data-aos-duration="1000">
                            <div>
                                {/*<span className="info__title">Mail me</span>*/}
                                <h4 className="info__desc">country: Armenia</h4>
                            </div>
                        </div>
                        <div className="info__item" data-aos="fade-right" data-aos-duration="1000">
                            <div>
                                <h4 className="info__desc">city: Gyumri</h4>
                            </div>
                        </div>
                        <div className="info__item" data-aos="fade-right" data-aos-duration="1000">
                            <div>
                                <h4 className="info__desc">Zip Code: 3104</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Contact;