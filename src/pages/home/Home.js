import React from 'react';
import Profile from '../../assets/narine.png'
import {Link} from "react-router-dom";
import {FaArrowRight} from "react-icons/fa";
import "./home.css"

const Home = () => {
    return (<section className="home section grid">
        <img src={Profile} alt="" className="home__img" data-aos="fade-out" data-aos-duration="1000"/>
        <div className="orange-slider-image-linear" data-aos="fade-out" data-aos-duration="1000"></div>
        <div className="home__content">
            <div className="home__data">
                <h2 className="home__title">
                    Web Developer
                    <br/>
                    <span> Narine Mkrtchyan </span>
                </h2>
                <p className="home__description" data-aos="fade-out" data-aos-duration="1000">
                    I'm Narine Mkrtchyan. I'm front-end and back-end developer focused on crafting clean & user-friendly
                    experiences. Iam
                    passionate about building excellent software that improves the lives of around me. Eager to learn
                    new technologies and improve skills.
                </p>
                <Link to='/about' className='button'>
                    More About Me
                    <span className='button__icon'>
                        <FaArrowRight/>
                    </span>
                </Link>
            </div>
        </div>
    </section>);
};

export default Home;