import React from 'react';
import SimpleSlider from "../../components/slick/SimpleSlider";
import './education.css'

const Education = () => {
    return (
        <section className="education section">
            <h2 className="section__title">Certificates & Diploma</h2>
            <div className="education__container" >
                <SimpleSlider/>
            </div>
        </section>
    );
};

export default Education;