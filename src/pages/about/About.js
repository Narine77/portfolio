import React from 'react';
import Info from "../../components/Info";
import {FaDownload} from "react-icons/fa";
import CV from "../../assets/NarineMkrtchyan.pdf"
import Stats from "../../components/Stats";
import Skills from "../../components/Skills";
import {resume} from "../../data";
import "./about.css"
import ResumeItem from "../../components/ResumeItem";


const About = () => {
    return (
        <div>
        <main className="section container about">
            {/*<div className="orange-slider-image-linear"></div>*/}
            <section>
                <h2 className="section__title">About <span>Me</span></h2>
                <div className="about__container grid">
                    <div className="about__info">
                        <h3 className="section__subtitle" data-aos="fade-right" data-aos-duration="1000">Personal Infos</h3>
                        <ul className='info__list grid'>
                            <Info/>
                        </ul>
                        <a href={CV} className='button'>
                            Download Cv <span className="button__icon"><FaDownload/></span>
                        </a>
                    </div>
                </div>
            </section>
            <div className="separator"></div>
            <section className="skills" data-aos="fade-up" data-aos-duration="1000">
                <h3 className="section__subtitle subtitle__center">My Skills</h3>
                <div className="skills__container grid">
                    <Skills/>
                </div>
            </section>
            <div className="separator"></div>
            <div className='stats grid'>
                <Stats/>
            </div>
            <div className="separator"></div>
            <section className="resume">
                <h3 className='section__subtitle subtitle__center'>
                    Experience & Education</h3>
                <div className="resume__container grid">
                    <div className="resume__data">
                        {resume.map((val) => {
                            if (val.category === 'experience') {
                                return <ResumeItem key={val.id} {...val} data-aos="fade-down" data-aos-duration="1000" />
                            }
                        })}
                    </div>
                    <div className="resume__data">
                        {resume.map((val) => {
                            if (val.category === 'education') {
                                return <ResumeItem key={val.id} {...val} />
                            }
                        })}
                    </div>
                </div>
            </section>
        </main>
        </div>
    );
};

export default About;