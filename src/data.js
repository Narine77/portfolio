import {
  FaHome,
  FaUser,
  FaFolderOpen,
  FaEnvelopeOpen,
  FaBriefcase,
  FaGraduationCap,
  FaCode,
} from 'react-icons/fa';
import { FiFileText } from 'react-icons/fi';

import Work1 from './assets/project-1.png';
import Work2 from  './assets/table.png';
import Work3 from './assets/project-3.png';
import Work4 from './assets/project-4.png';
import Img1 from './assets/diploma1.jpg';
import Img2 from './assets/php.jpg';
import Img3 from './assets/js.jpg';
import Img4 from './assets/html.jpg';
import Img5 from './assets/react.jpg';

export const links = [
  {
    id: 1,
    name: 'Home',
    icon: <FaHome className='nav__icon' />,
    path: '/',
  },

  {
    id: 2,
    name: 'About',
    icon: <FaUser className='nav__icon' />,
    path: '/about',
  },

  {
    id: 3,
    name: 'Portfolio',
    icon: <FaFolderOpen className='nav__icon' />,
    path: '/portfolio',
  },

  {
    id: 4,
    name: 'Contact',
    icon: <FaEnvelopeOpen className='nav__icon' />,
    path: '/contact',
  },
  {
    id: 5,
    name: 'Education',
    icon: <FaGraduationCap className='nav__icon' />,
    path: '/education',
  },
];

export const personalInfo = [
  {
    id: 1,
    title: 'First Name : ',
    description: 'Narine',
  },

  {
    id: 2,
    title: 'Last Name : ',
    description: 'Mkrtchyan',
  },

  {
    id: 3,
    title: 'Nationality : ',
    description: 'Armenian',
  },

  {
    id: 4,
    title: 'Freelance : ',
    description: 'Available',
  },

  {
    id: 5,
    title: 'Address : ',
    description: 'Armenia',
  },


  {
    id: 6,
    title: 'Languages : ',
    description: 'Russian, English',
  },
];

export const stats = [
  {
    id: 1,
    no: '3+',
    title: 'Years of Experience',
  },

  {
    id: 2,
    no: '7+',
    title: 'Completed Projects',
  },

];

export const resume = [
  {
    id: 1,
    category: 'experience',
    icon: <FaBriefcase />,
    year: '2023- Present',
    title: ' Freelance <span> Remote</span>',
  },
  {

    id: 2,
    category: 'experience',
    icon: <FaBriefcase />,
    year: '2022',
    title: 'Fullstack Developer <span> gHost </span>',
  },

  {
    id: 3,
    category: 'experience',
    icon: <FaBriefcase />,
    year: '2019-2020',
    title: 'Frontend Developer <span> LifeBeget </span>',
  },


  {
    id: 4,
    category: 'education',
    icon: <FaGraduationCap />,
    year: '2021',
    title: 'Engineering Degree <span>Techno-Educational-Academy </span>',
  },

  {
    id: 5,
    category: 'education',
    icon: <FaGraduationCap />,
    year: '2019',
    title: 'Engineering Degree<span> Gyumri Technology Center </span>',
  },

  {
    id: 6,
    category: 'education',
    icon: <FaGraduationCap />,
    year: '2006',
    title: 'Masters Degree <span> Gyumri State Institute </span>',
  },
];

export const skills = [
  {
    id: 1,
    title: 'Html',
    percentage: '90',
  },

  {
    id: 2,
    title: 'Css',
    percentage: '90',
  },

  {
    id: 3,
    title: 'Javascript',
    percentage: '75',
  },

  {
    id: 4,
    title: 'Jquery',
    percentage: '65',
  },

  {
    id: 5,
    title: 'React',
    percentage: '65',
  },
  {
    id: 6,
    title: 'TypeScript',
    percentage: '55',
  },
  {
    id: 7,
    title: 'Docker',
    percentage: '60',
  },
  {
    id: 8,
    title: 'Git',
    percentage: '65',
  },
  {
    id: 9,
    title: 'Node',
    percentage: '70',
  },
  {
    id: 10,
    title: 'Express',
    percentage: '65',
  },
  {
    id: 11,
    title: 'MySql',
    percentage: '60',
  },
  {
    id: 12,
    title: 'Nest',
    percentage: '55',
  },
];

export const portfolio = [
  {
    id: 1,
    img: Work1,
    title: 'Page creating',
    details: [
      {
        icon: <FiFileText />,
        title: 'Hostkey : ',
        desc: 'Page',
      },
      {
        icon: <FaCode />,
        title: 'Language : ',
        desc: 'Html, Js',
      },
    ],
  },

  {
    id: 2,
    img: Work2,
    title: 'Table Creating',
    details: [
      {
        icon: <FiFileText />,
        title: 'Project : ',
        desc: 'Website',
      },
      {
        icon: <FaCode />,
        title: 'Language : ',
        desc: 'React JS',
      },
    ],
  },

  {
    id: 3,
    img: Work3,
    title: 'Slider Creating',
    details: [
      {
        icon: <FiFileText />,
        title: 'Hostkey : ',
        desc: 'Slider',
      },
      {
        icon: <FaCode />,
        title: 'Language : ',
        desc: 'Html, JQuery',
      },
    ],
  },

  {
    id: 4,
    img: Work4,
    title: 'Admin creating',
    details: [
      {
        icon: <FiFileText />,
        title: 'Project : ',
        desc: 'Sortpak',
      },
      {
        icon: <FaCode />,
        title: 'Language : ',
        desc: 'React JS',
      },
    ],
  }
];

export const certificates  = [
  {
    id: 1,
    img: Img1,
  },
  {
    id: 2,
    img: Img2,
  },
  {
    id: 3,
    img: Img3,
  }, {
    id: 4,
    img: Img4,
  }, {
    id: 5,
    img: Img5,
  }
]