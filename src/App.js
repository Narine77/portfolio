
import './App.css';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Home from "./pages/home/Home";
import About from "./pages/about/About";
import Portfolio from "./pages/portfolio/Portfolio";
import Contact from "./pages/contact/Contact";
import Navbar from "./components/Navbar";
import Themes from "./components/Themes";
import Education from "./pages/certificate/Education";

function App() {
  return (
    <BrowserRouter>
      <Navbar/>
        <Themes/>
      <Routes>
        <Route path="/" element ={<Home/>}/>
        <Route path="/about" element ={<About/>}/>
        <Route path="/contact" element ={<Contact/>}/>
        <Route path="/portfolio" element ={<Portfolio/>}/>
        <Route path="/education" element ={<Education/>}/>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
